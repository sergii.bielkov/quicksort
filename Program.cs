﻿using System;
using System.Diagnostics;

namespace QuickSort
{
    class Program
    {
        static void Main(string[] args)
        {
            // Time for 10 elements random array is 0 milliseconds
            // Time for 100 elements random array is 0 milliseconds
            // Time for 1000 elements random array is 0 milliseconds
            // Time for 10, 000 elements random array is 1 milliseconds
            // Time for 100, 000 elements random array is 15 milliseconds
            // Time for 1, 000, 000 elements random array is 0,208 sec
            // Time for 10, 000, 000 elements random array is 6,567 sec
            // Time for 100, 000, 000 elements random array is 9,1 minutes

            var stopwatch = new Stopwatch();

            {
                var testAmount = 10;
                var testArray = GenerateRandomArrayOfSize(testAmount);
                stopwatch.Reset();
                stopwatch.Start();
                QuickSort(testArray, 0, testArray.Length - 1);
                stopwatch.Stop();
                Console.WriteLine(
                    $"Time for {testAmount} elements random array is {((double) stopwatch.ElapsedMilliseconds)} milliseconds");
            }

            {
                var testAmount = 100;
                var testArray = GenerateRandomArrayOfSize(testAmount);
                stopwatch.Reset();
                stopwatch.Start();
                QuickSort(testArray, 0, testArray.Length - 1);
                stopwatch.Stop();
                Console.WriteLine(
                    $"Time for {testAmount} elements random array is {((double) stopwatch.ElapsedMilliseconds)} milliseconds");
            }

            {
                var testAmount = 1000;
                var testArray = GenerateRandomArrayOfSize(testAmount);
                stopwatch.Reset();
                stopwatch.Start();
                QuickSort(testArray, 0, testArray.Length - 1);
                stopwatch.Stop();
                Console.WriteLine(
                    $"Time for {testAmount} elements random array is {((double)stopwatch.ElapsedMilliseconds)} milliseconds");
            }

            {
                var testAmount = 10000;
                var testArray = GenerateRandomArrayOfSize(testAmount);
                stopwatch.Reset();
                stopwatch.Start();
                QuickSort(testArray, 0, testArray.Length - 1);
                stopwatch.Stop();
                Console.WriteLine(
                    $"Time for 10,000 elements random array is {((double)stopwatch.ElapsedMilliseconds)} milliseconds");
            }

            {
                var testAmount = 100000;
                var testArray = GenerateRandomArrayOfSize(testAmount);
                stopwatch.Reset();
                stopwatch.Start();
                QuickSort(testArray, 0, testArray.Length - 1);
                stopwatch.Stop();
                Console.WriteLine(
                    $"Time for 100,000 elements random array is {((double)stopwatch.ElapsedMilliseconds)} milliseconds");
            }

            {
                var testAmount = 1000000;
                var testArray = GenerateRandomArrayOfSize(testAmount);
                stopwatch.Reset();
                stopwatch.Start();
                QuickSort(testArray, 0, testArray.Length - 1);
                stopwatch.Stop();
                Console.WriteLine(
                    $"Time for 1,000,000 elements random array is {((double)stopwatch.ElapsedMilliseconds) / 1000} sec");
            }

            {
                var testAmount = 10000000;
                var testArray = GenerateRandomArrayOfSize(testAmount);
                stopwatch.Reset();
                stopwatch.Start();
                QuickSort(testArray, 0, testArray.Length - 1);
                stopwatch.Stop();
                Console.WriteLine(
                    $"Time for 10,000,000 elements random array is {((double)stopwatch.ElapsedMilliseconds) / 1000} sec");
            }

            {
                var testAmount = 100000000;
                var testArray = GenerateRandomArrayOfSize(testAmount);
                stopwatch.Reset();
                stopwatch.Start();
                QuickSort(testArray, 0, testArray.Length - 1);
                stopwatch.Stop();
                Console.WriteLine(
                    $"Time for 100,000,000 elements random array is {((double)stopwatch.ElapsedMilliseconds) / 1000} sec");
            }
        }

        public static void QuickSort(int[] dataArray, int leftIndex, int rightIndex)
        {
            if (leftIndex == rightIndex)
            {
                return;
            }

            if (dataArray.Length == 2 && dataArray[0] > dataArray[1])
            {
                var temp = dataArray[0];
                dataArray[0] = dataArray[1];
                dataArray[1] = temp;
                return;
            }

            var pivotIndex = NewMethod(dataArray, leftIndex, rightIndex);

            if (pivotIndex > leftIndex)
            {
                QuickSort(dataArray, leftIndex, pivotIndex - 1);
            }

            if (pivotIndex < rightIndex)
            {
                QuickSort(dataArray, pivotIndex + 1, rightIndex);
            }
        }

        private static int NewMethod(int[] dataArray, int leftIndex, int rightIndex)
        {
            var pivotIndex = (leftIndex + rightIndex) / 2;
            var pivot = dataArray[pivotIndex];

            while (true)
            {
                while (dataArray[leftIndex] <= pivot && leftIndex < pivotIndex)
                {
                    leftIndex++;
                }

                while (dataArray[rightIndex] >= pivot && rightIndex > pivotIndex)
                {
                    rightIndex--;
                }

                if (leftIndex == pivotIndex && rightIndex == pivotIndex)
                {
                    break;
                }

                var tempItemData = dataArray[leftIndex];
                dataArray[leftIndex] = dataArray[rightIndex];
                dataArray[rightIndex] = tempItemData;

                if (leftIndex == pivotIndex)
                {
                    pivotIndex = rightIndex;
                    continue;
                }

                if (rightIndex == pivotIndex)
                {
                    pivotIndex = leftIndex;
                }
            }

            return pivotIndex;
        }

        public static int[] GenerateRandomArrayOfSize(int arraySize)
        {
            var random = new Random();
            var array = new int[arraySize];
            for (int index = 0; index < arraySize; index++)
            {
                array[index] = random.Next(10000);
            }

            return array;
        }
    }
}
